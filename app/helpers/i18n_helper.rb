module I18nHelper

  def t_model model
    model.to_s.camelize.constantize.model_name.human
  end

  def t_model_plural model
    model.to_s.camelize.constantize.model_name.human(count: 2)
  end

  def t_model_attribute model, attribute
    model.to_s.camelize.constantize.human_attribute_name attribute
  end

end
