class Notifier < ActionMailer::Base
  default from: "test.mail.seseg@gmail.com"

  def send_order(object, args={}, *atachments)
    @order = object
    mail({
      to: object.user.email,
      subject: args[:subject] || 'NF-e',
      charset: args[:charset] || 'utf-8',
      :parts_order => args[:parts_order] || [],
      content_type: args[:content_type] || 'text/html'
      })
  end

end
