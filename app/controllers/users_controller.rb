class UsersController < ApplicationController

  respond_to :html, :json

  def new
    respond_with @user = User.new
  end

  def show
    respond_with @user = User.find(params[:id])
  end

  def create
    respond_with @user = User.create(params[:user])
  end

  def index
    respond_with @users = User.all
  end

  def destroy
    respond_with @user = User.destroy(params[:id])
  end

  def edit
    respond_with @user = User.find(params[:id])
  end

  def update
    respond_with @user = User.update(params[:id], params[:user])
  end

end
