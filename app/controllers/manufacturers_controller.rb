class ManufacturersController < ApplicationController

  respond_to :html

  def new
    @manufacturer = Manufacturer.new
  end

  def create
    respond_with @manufacturer = Manufacturer.create(params[:manufacturer])
  end

  def show
    respond_with @manufacturer = Manufacturer.find(params[:id])
  end

  def edit
    @manufacturer = Manufacturer.find(params[:id])
  end

  def update
    respond_with @manufacturer = Manufacturer.update(params[:id], params[:manufacturer])
  end

  def index
    respond_with @manufacturers = Manufacturer.all
  end

  def destroy
    respond_with @manufacturer = Manufacturer.destroy(params[:id])
  end

  def map_manufacturers
    Manufacturer.all
  end
  helper_method :map_manufacturers

end