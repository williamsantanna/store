class OffersController < ApplicationController

  respond_to :html, :json

  def index
    respond_with @products = Product.order('name').page(params[:page])
  end

end
