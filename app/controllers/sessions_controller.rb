class SessionsController < ApplicationController

  respond_to :html, :json

  def new
    respond_with @session = User.new
  end

  def create

    @session = User.find_by_email(params[:user][:email])

    if @session && User.authenticate(@session.email, params[:user][:password])
      session[:email_id] = @session.id
      redirect_to users_path, {:notice => "#{@session.email} was logged with success!"}

    else
      redirect_to new_session_path, {:notice => "Invalid user or password!"}
    end

  end

  def destroy
    session[:email_id] = nil
    redirect_to users_path
  end

end
