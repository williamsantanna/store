class ProductsController < ApplicationController

  respond_to :html

  def new
    respond_with @product = Product.new
  end

  def create
    respond_with @product = Product.create(params[:product])
  end

  def show
    respond_with @product = Product.find(params[:id])
  end

  def index
    respond_with @products = Product.order('name').page(params[:page])
  end

  def destroy
    respond_with @product = Product.destroy(params[:id])
  end

  def edit
    respond_with @product = Product.find(params[:id])
  end

  def update
    respond_with @product = Product.update(params[:id], params[:product])
  end

end
