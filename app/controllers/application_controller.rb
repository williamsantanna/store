class ApplicationController < ActionController::Base
  protect_from_forgery

  def map_items_cart #devera retornar uma instancia do array de cart
    Product.order(:name).find( session[:cart] )#buscara no banco somente os produtos que estiverem no array pelo id de cada
    # produto e o retorno sera uma instancia com todos os elementos deste array onde sera possivel acessar os atributos destes
    # objetos tanto pelo id quanto pelo nome na pagina de index onde o metodo e chamado.
  end

  def current_user
    User.find(session[:email_id]) if session[:email_id]
  end

  helper_method :map_items_cart, :current_user

end
