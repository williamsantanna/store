class OrdersController < ApplicationController

  respond_to :html

  def show
    respond_with @order = Order.find(params[:id])
  end

end
