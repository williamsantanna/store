class CartController < ApplicationController

  respond_to :html, :json, :pdf

  def add_product #metodo para adicionar produto no carrinho

    (session[:cart] ||= []) << params[:id] #se variavel session[:cart] estiver vazia recebera array vazio
    p "cart items: #{session[:cart]}"   # imprime a secao
    render :nothing => true  #necessita renderizar algo na tela senao da erro

  end

  def destroy

    session[:cart].delete(params[:id])  # na secao do cart chama o metodo delete passando o id
    p "cart items: #{session[:cart]}" # imprime na tela a secao
    render :nothing => true #necessita renderizar algo na tela senao da erro

  end

  def checkout
    if session[:email_id]
      @order = Order.create user_id: session[:email_id]

      session[:cart].map do |i|
        Item.create order_id: @order.id, product_id: i, quantity: params["#{:product}_#{i}"][:quantity]
      end
      Notifier.send_order(@order).deliver
      respond_with @order
    else
      redirect_to new_session_path
    end
  end

  def show
    @order = Order.new
    session[:cart].map do |i|
      @order.items.build product_id: i
    end
    render 'show'
  end

end