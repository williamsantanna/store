class SessionController < ApplicationController

  respond_to :html, :json

  def new
    @session = User.new
  end

  def create

    @session = User.find_by_email(params[:user][:email])

    if @session && User.authenticate(@session.email, params[:user][:password])
      session[:email_id] = @session.id
      redirect_to users_path
    else
      respond_with @session, location: new_session_path
    end
  end

  def destroy
    session[:email_id] = nil
    redirect_to new_session_path
  end

end
