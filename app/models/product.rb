class Product < ActiveRecord::Base

  belongs_to :manufacturer
  has_many :items

  attr_accessible :description, :name, :photo, :manufacturer_id

  mount_uploader :photo, ImageUploader

  paginates_per 10

  # validates_presence_of :manufacturer_id

end
