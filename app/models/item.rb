class Item < ActiveRecord::Base

  attr_accessible :quantity, :product_id, :order_id

  belongs_to :order

  belongs_to :product

end
