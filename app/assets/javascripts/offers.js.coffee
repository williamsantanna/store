id_extract = (id) ->
  id = id.split('_')
  id[id.length-1]

set_remove_item = ->
  # sera criado somente se houver um li
  $('#cart ol li a').click ->
    parent_node = $(this).parent()
    $.ajax
      type: "DELETE"
      url: "/cart/items/" + id_extract(parent_node.attr('id'))
    parent_node.remove()

    unless $('#cart ol li').length
      $('#cart a#cart').remove()

set_checkout = ->
  a = $('<a>'
    text: 'cart'
    href: '/cart'
    id: 'cart'
  )

  if $('#cart ol li').length
    unless $('#cart a#cart').length
      a.appendTo $('#cart')


$ ->
  $('.draggable').draggable
    helper: 'clone'

  $('#cart').droppable
    drop: (event, ui) ->
      li = $('<li></li>')

      for product in $('#cart ol li')
        if ui.draggable.attr('id') == product.id
          duplicity = true
          break

      if duplicity
        alert('This product has already been added!')
      else
        li
          .appendTo( $('#cart ol') )
          .text( ui.draggable.text() )
          .attr('id', ui.draggable.attr('id'))
          .append(
            $('<a>'
              href: '#'
              text: 'x'
            )
          )
          set_remove_item()
          set_checkout()

        $.ajax
          type: "POST"
          url: "/cart/items/" + id_extract(li.attr('id'))

  set_remove_item()
  set_checkout()