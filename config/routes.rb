RegisterProductManufacturer::Application.routes.draw do

  resources :products
  resources :manufacturers
  resources :users
  resources :session, :only => [:new, :create, :destroy]
  resources :offers, :only => [:index, :show]
  resources :orders
  resources :items

  post 'cart/items/:id' => 'cart#add_product'
  delete 'cart/items/:id' => 'cart#destroy'
  get 'cart' => 'cart#show'
  post 'checkout' => 'cart#checkout'

end
