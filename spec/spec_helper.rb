# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara'
require 'fabrication/syntax/make'


# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

# Capybara.javascript_driver = :webkit

Capybara.register_driver :selenium do |app|

  $browser = Capybara::Selenium::Driver.new(app, :browser => :firefox)
  # $browser = Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

module HTMLAction

  module Fill

    def fill_fields(html_object_id, args)
      within html_object_id do
        args.each_pair{|key, value| fill_in(key.to_s.capitalize, with: value) }
      end
    end

    def fill_login(email, password, form_id="form")
      within form_id do
        fill_in "Email", with: 'william.santanna28@gmail.com'
        fill_in "Password", with: '081254452180'
      end
    end

  end

  module Drag
    def drag_to(element, target)
      find(element).drag_to( find(target) )
    end
  end

  module Click
    # dessa forma fica mais genérico
    def click_link_on(text, father_id)
      find("#{father_id} a", text: text).click
    end
  end

  module Page

    module Label
      def label_should_contain(model, contents)
        contents.each_pair { |key, value| page.should have_content "#{t_model_attribute(model, key)}: #{value}" }
      end

      def label_should_not_contain(model, contents)
        contents.each_pair { |key, value| page.should have_no_content "#{t_model_attribute(model, key)}: #{value}" }
      end

      def page_should_contain(*contents)
        contents.each{ |c| page.should have_content c }
      end
    end
  end
end

module Create

  def create_products! amount=1
    Product.make_list! amount, manufacturer_id: Manufacturer.make!.id
  end

  def create_user_login(email, password)
    User.create({:password => password, :email => email})
  end

  def sign_in (email, password)
    visit new_session_path
    fill_login(email, password)
    click_button "Sign In"
  end

end


RSpec.configure do |config|

  config.include HTMLAction::Drag
  config.include HTMLAction::Fill
  config.include HTMLAction::Click
  config.include HTMLAction::Page
  config.include HTMLAction::Page::Label
  config.include HTMLAction::Page::HTML
  config.include Create

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.use_transactional_fixtures = true

  config.infer_base_class_for_anonymous_controllers = false

  config.order = "random"

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    sleep(1)
    DatabaseCleaner.clean
    #DatabaseCleaner.reset_database
  end

end
