require 'spec_helper'

describe 'Offer Show.' do

  let!(:product){ create_products!(amount=2).sample }

  it 'Should visit offers index, click on Show link and show one offer' do

    visit offers_path

    # p product

    click_link_on product.name, "#product_#{product.id}"
    # p product.manufacturer.name
    page_should_contain product.name
    label_should_contain :product, manufacturer: product.manufacturer.name
    # label_should_contain :product, description: product.description
    save_and_open_page
  end

  # it 'Should visit show url and show a product' do

  #   visit product_path(product.id)
  #   page_should_contain product.name
  #   label_should_contain :product, manufacturer: product.manufacturer.name
  #   label_should_contain :product, description: product.description
  #   # save_and_open_page
  # end

end