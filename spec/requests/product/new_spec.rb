require 'spec_helper'

describe 'Product Create' do

  it 'Should create a product' do

    manufacturer = Manufacturer.make_list!(2).sample
    product = Product.make manufacturer_id: manufacturer.id
    visit new_product_path
    fill_in "Name", with: "ps3"
    fill_in "Description", with: "120gb"
    src = Rails.root.join 'app', 'assets', 'images', 'rails.png'
    attach_file('photo', src)
    click_button "Save"
    page.should have_content "ps3"
    page.should have_content "120gb"
    find('img', src: src)

  end

end
