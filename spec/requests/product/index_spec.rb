require 'spec_helper'

describe 'List products' do
  it 'Should list products' do
    manufacturer = Manufacturer.make!
    products = Product.make_list! 3, manufacturer_id: manufacturer.id
    visit products_path
    page.should have_content "Product 2"
  end
end
