require 'spec_helper'

describe 'Show a product' do

  let!(:product){ create_products!(2).sample }
  let!(:photo_path){ Rails.root.join 'app', 'assets', 'images' }
  let!(:file_name){ 'rails.png' }

  it 'Should show a product' do

    product.photo = File.open(photo_path.join file_name)
    product.save
    visit products_path
    click_link "show"
    page.should have_content product.name[0]
    page.should have_content product.description[0]
    find 'img', src: photo_path.join(file_name)
  end
end
