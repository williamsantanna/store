require 'spec_helper'

describe 'Product destroy.' do

  it 'Should destroy a product' do

    manufacturer = Manufacturer.make!
    products = Product.make_list! 3, manufacturer_id: manufacturer.id
    src = '/home/william/pictures'
    file_name = 'ps3.jpeg'
    photo = File.open('/home/william/pictures/ps3.jpeg')
    visit products_path
    click_link "delete"
    page.should have_no_content "Product 0"

  end
end
