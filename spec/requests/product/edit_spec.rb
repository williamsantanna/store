require 'spec_helper'

describe 'Edit a product' do

  it 'Should edit a product' do

    src = '/home/william/pictures/ps3.jpeg'
    manufacturer = Manufacturer.make!
    product = Product.make! manufacturer_id: manufacturer.id
    product.photo = File.open(src)
    product.save

    visit products_path

    click_link "edit"

    new_manufacturer = manufacturer.name
    select(new_manufacturer, from: 'Manufacturer')
    fill_in "Name", with: "PS3"
    fill_in "Description", with: "320gb"
    attach_file('photo', src)

    click_button "Update"

    page.should have_content "PS3"
    page.should have_content "320gb"
    find('img', src: src)
  end
end
