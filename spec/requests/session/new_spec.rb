require 'spec_helper'

describe 'sign in' do

  # it 'should access with a valid user' do
  #   user = create_user_login('william.santanna28@gmail.com', '081254452180')
  #   sign_in user.email, user.password
  #   page.should have_content "william.santanna28@gmail.com"
  # end



  it 'should try access with a invalid user' do
    user = create_user_login('william.santanna28@gmail.com', '081254452180')
    sign_in 'william.santanna28@gmail.com', '081254452180'
    save_and_open_page
    page.should have_no_css "#session", :text => user.email
    # page.should have_content 'Invalid user or password!'
    # save_and_open_page
  end



  # it 'should try access with a invalid password' do

  #   user = create_user_login('william.santanna28@gmail.com', '081254452180')

  #   sign_in 'william.santanna28@gmail.com', '081254'
  #   save_and_open_page
  #   page.should have_content 'Invalid user or password!'

  #   # save_and_open_page

  # end

end






# describe 'Sign in. ' do
#   let!(:password){'123'}

#   it 'Should access with a valid general user' do
#     user = User.make! password: password
#     sign_in :user, email: user.email, password: user.password

#     page.should have_css "#session", :text => user.email
#     # save_and_open_page
#   end

#   it 'Should try access with a invalid user' do
#     user = User.make! password: password
#     # p user.email
#     sign_in :user, password: user.password, email: "x_#{user.email}"
#     page.should have_no_css "#session", :text => user.email
#     # save_and_open_page
#   end

#   it 'Should try access with a invalid password' do
#     user = User.make! password: password
#     sign_in :user, email: user.email, password: "#{user.password}1"
#     page.should have_no_css "#session", :text => user.email
#     # save_and_open_page
#   end


# end