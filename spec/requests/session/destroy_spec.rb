require 'spec_helper'

describe 'Sign out' do

  it 'Should sign out' do

    user = create_user_login('william.santanna28@gmail.com', '081254452180')

    sign_in user.email, user.password

    visit users_path

    click_link "Sign Out"

    page.should have_no_css '#session_id', :text => user.email

    # save_and_open_page

  end

end
