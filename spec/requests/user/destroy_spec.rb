require 'spec_helper'

describe 'Create new user' do
  it 'Should create new user' do
    user = User.make!
    visit users_path
    click_link 'delete'
    page.should have_no_content 'william.santanna281@gmail.com'
  end
end
