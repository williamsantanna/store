require 'spec_helper'

describe 'list of users' do
  it 'Should list all users' do
    user = User.make_list! 3
    visit users_path
    page.should have_content "william.santanna282@gmail.com"
  end
end