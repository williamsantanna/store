require 'spec_helper'

describe 'Should a user' do
  it 'Should show a user' do
    user = User.make!
    visit users_path
    click_link 'show'
    page.should have_content 'william.santanna280@gmail.com'
  end
end
