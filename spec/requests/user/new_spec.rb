require 'spec_helper'

describe 'Create new user' do
  it 'Should create new user' do
    visit new_user_path
    within 'form' do
      fill_in "Email", with: "william_santanna28@gmail.com"
      fill_in "Password", with: "123"
      fill_in "Password confirmation", with: "123"
    end
    click_button 'Save'
    page.should have_content "william_santanna28@gmail.com"
  end
end
