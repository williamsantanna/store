require 'spec_helper'

describe 'Edit a user' do
  it 'Should edit a user' do
    user = User.make!
    visit edit_user_path user
    within 'form' do
      fill_in "Email", with: "william.santanna290@gmail.com"
      fill_in "Password", with: "123"
      fill_in "Password confirmation", with: "123"
    end
    click_button "Update"
    page.should have_content "william.santanna290@gmail.com"
  end
end
