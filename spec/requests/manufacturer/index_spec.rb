require 'spec_helper'

describe 'List all manufacturers' do
  it 'Should list all manufacturers' do
    manufacturer = Manufacturer.make_list! 5
    visit manufacturers_path
    page.should have_content manufacturer[1].name
  end
end