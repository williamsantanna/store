require 'spec_helper'

describe 'Show a manufacturer' do
  let!(:manufacturer){ Manufacturer.make! }
  it 'Should show a manufacturer' do
  visit manufacturers_path
  page.should have_content "#{manufacturer.name}"
  end
end
