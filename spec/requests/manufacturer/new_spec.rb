require 'spec_helper'

describe 'Create a manufacturer' do
  it 'Should create a manufacturer' do
    visit new_manufacturer_path
    fill_in "Name", with: "sony"
    click_button "Save"
    page.should have_content "sony"
  end
end
