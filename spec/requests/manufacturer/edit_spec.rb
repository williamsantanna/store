require 'spec_helper'

describe 'Edit a manufacturer' do
  it 'Should edit a manufacturer' do
    manufacturer = Manufacturer.create name:"ipad"
    visit manufacturers_path
    click_link "edit"
    fill_in "Name", with: "samsung"
    click_link "List"
  end
end
