require 'spec_helper'

describe 'Destroy a manufacturer' do
  let!(:manufacturer){Manufacturer.create :name => "google"}
  it 'Should destroy a manufacturer' do
    visit manufacturers_path
    click_link "delete"
    page.should have_no_content "google"
  end
end
