class ActiveRecord::Base

  class << self

    # opts, force expected hash
    def make_list amount, opts = {}
      # make é um método criado na ActiveRecord,
      amount.times.map{ make opts }
    end

    def make_list! amount, opts = {}
      amount.times.map{ make! opts }
    end

  end

end
