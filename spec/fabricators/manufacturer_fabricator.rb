Fabricator(:manufacturer) do
  name { sequence(:name) { |n| "Manufacturer #{n}" } }
end