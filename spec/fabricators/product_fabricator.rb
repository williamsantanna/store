Fabricator(:product) do
  name { sequence(:name) { |n| "Product #{n}" } }
  description { sequence(:description) { |n| "description #{n}" } }
end
